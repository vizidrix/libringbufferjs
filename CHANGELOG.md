# Change Log

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).
We strive to follow [Change Log Standards](http://keepachangelog.com/).

## [Unreleased]

- Edited
  - Added benchmarks to dev watch script

## [0.1.0] - 2015-01-09

- Added
  - Initial implementation of RingBuffer, ObjectRingBuffer and BinaryRingBuffer
  - NPM scripts for compile, unit test, benchmark, docs and dev mode (with watcher)
  - Documentation of library and supporting material in README.md
  - TSDoc documentation coverage for all major methods
  - 100% starting code coverage using Tape / Blue-Tape
  - Initial suite of benchmarks to establish baseline performance metrics with BenchmarkJS
  - GitLab CI configuration to run tests and benchmarks on check-in
  - TSLint configuration to standardize style guidelines

## [0.1.1] - 2015-01-10

- Edited
  - Updated TypeScript related linkings to fix NPM package
  - Minor updates/fixes to documentation

- Removed
  - Build step config for GitLab Pages due to failing step
