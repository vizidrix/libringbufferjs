import { Event, Suite } from 'benchmark'

import {
  RingBuffer,
  ObjectRingBuffer,
  BinaryRingBuffer,
}   from '../src/libringbufferjs'

let runner = (suite: Suite) => {
  suite
  .on('cycle', (e: Event) => {
    console.log('Finished: ', String(e.target))
  })
  .on('complete', function() {
    let fastest = this.filter('fastest');
    console.log('Fastest is ', fastest.pluck('name'))
  })
  // .run({ 'async': false })

  return {
    run: () => suite.run({ 'async': false })
  }
}

let suiteDataViewNewEachRequest = (): Function => {
  let buffer = new ArrayBuffer(16 * 16)
  let index = 0
  return () => {
    let view = new DataView(buffer, index++ & 15, 16)
    view.setInt8(0, 10)
  }
}

let suiteDataViewCachedViews = (): Function => {
  let buffer = new ArrayBuffer(16 * 16)
  let index = 0
  let cache: Array<DataView> = []
  for (let i = 0; i < 16; i++) {
    cache.push(new DataView(buffer, i * 16, 16))
  }
  return () => {
    let view = cache[index++ & 15]
    view.setInt8(0, 10)
  }
}

let suiteDataView = new Suite()
  .add('suite - DataView - NewEachRequest', suiteDataViewNewEachRequest())
  .add('suite - DataView - CachedViews', suiteDataViewCachedViews())

let suiteClosuresVariableStoredFunction = (): Function => {
  let value = 0
  let increment = () => {
    value++
  }
  let state = {
    incrementor: increment
  }
  return () => {
    state.incrementor()
  }
}

let suiteClosuresAnonymousFunction = (): Function => {
  let value = 0
  let state = {
    incrementor: () => value++
  }
  return () => {
    state.incrementor()
  }
}

let suiteClosures = new Suite()
  .add('suite - Closures - VariableStoredFunction', suiteClosuresVariableStoredFunction())
  .add('suite - Closures - AnonymousFunction', suiteClosuresAnonymousFunction())

let suiteBinaryRingBufferClaimFillPeekLoop32x32 = (): Function => {
  let rb = BinaryRingBuffer(32, 32)
  return () => {
    let writer = rb.claim()
    writer.setInt32(0, 10)
    writer.setInt32(4, 20)
    writer.setInt32(8, 30)

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteBinaryRingBufferClaimFillPeekLoop64x32 = (): Function => {
  let rb = BinaryRingBuffer(64, 32)
  let index = 0
  return () => {
    index++
    let writer = rb.claim()
    writer.setInt32(0, 10)
    writer.setInt32(4, 20)
    writer.setInt32(8, 30)

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteBinaryRingBufferClaimFillPeekLoop128x32 = (): Function => {
  let rb = BinaryRingBuffer(128, 32)
  let index = 0
  return () => {
    index++
    let writer = rb.claim()
    writer.setInt32(0, 10)
    writer.setInt32(4, 20)
    writer.setInt32(8, 30)

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteBinaryRingBufferClaimFillPeekLoop = new Suite()
  .add('suite - BinaryRingBuffer - ClaimFillPeekLoop - 32x32', suiteBinaryRingBufferClaimFillPeekLoop32x32())
  .add('suite - BinaryRingBuffer - ClaimFillPeekLoop - 64x32', suiteBinaryRingBufferClaimFillPeekLoop64x32())
  .add('suite - BinaryRingBuffer - ClaimFillPeekLoop - 128x32', suiteBinaryRingBufferClaimFillPeekLoop128x32())

let suiteRingBufferClaimFillPeekLoop32 = (): Function => {
  let rb = RingBuffer(32, () => {return {value: 0}})
  return () => {
    let writer = rb.claim()
    writer.value = 10

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteRingBufferClaimFillPeekLoop64 = (): Function => {
  let rb = RingBuffer(64, () => {return {value: 0}})
  return () => {
    let writer = rb.claim()
    writer.value = 10

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteRingBufferClaimFillPeekLoop128 = (): Function => {
  let rb = RingBuffer(128, () => {return {value: 0}})
  return () => {
    let writer = rb.claim()
    writer.value = 10

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteRingBufferClaimFillPeekLoop = new Suite()
  .add('suite - RingBuffer - ClaimFillPeekLoop - 32', suiteRingBufferClaimFillPeekLoop32)
  .add('suite - RingBuffer - ClaimFillPeekLoop - 64', suiteRingBufferClaimFillPeekLoop64)
  .add('suite - RingBuffer - ClaimFillPeekLoop - 128', suiteRingBufferClaimFillPeekLoop128)

let suiteObjectRingBufferClaimFillPeekLoop32 = (): Function => {
  let rb = ObjectRingBuffer(32, () => {return {value: 0}})
  return () => {
    let writer = rb.claim()
    writer.value = 10

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteObjectRingBufferClaimFillPeekLoop64 = (): Function => {
  let rb = ObjectRingBuffer(64, () => {return {value: 0}})
  return () => {
    let writer = rb.claim()
    writer.value = 10

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteObjectRingBufferClaimFillPeekLoop128 = (): Function => {
  let rb = ObjectRingBuffer(128, () => {return {value: 0}})
  return () => {
    let writer = rb.claim()
    writer.value = 10

    rb.peek(rb.getPosition() - 1)
  }
}

let suiteObjectRingBufferClaimFillPeekLoop = new Suite()
  .add('suite - ObjectRingBuffer - ClaimFillPeekLoop - 32', suiteObjectRingBufferClaimFillPeekLoop32)
  .add('suite - ObjectRingBuffer - ClaimFillPeekLoop - 64', suiteObjectRingBufferClaimFillPeekLoop64)
  .add('suite - ObjectRingBuffer - ClaimFillPeekLoop - 128', suiteObjectRingBufferClaimFillPeekLoop128)

runner(suiteDataView)// .run()
runner(suiteClosures)// .run()
runner(suiteBinaryRingBufferClaimFillPeekLoop).run()
runner(suiteRingBufferClaimFillPeekLoop).run()
runner(suiteObjectRingBufferClaimFillPeekLoop).run()
