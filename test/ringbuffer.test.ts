import * as test from 'blue-tape'

import {
  RingBuffer,
  ObjectRingBuffer,
  BinaryRingBuffer
}   from '../src/libringbufferjs'

test('RingBuffer<T>', suite => {
  interface ITestElement {
    value: number
  }

  suite.test('should create a buffer of proper length', assert => {
    let rb = RingBuffer<ITestElement>(4, () => {return { value: 0 }})
    assert.equal(rb.getRingSize(), 4, 'be a 4 slice ring')
    assert.end()
  })

  suite.test('should not reset elements if no reset is provided', assert => {
    let rb = RingBuffer<ITestElement>(4, () => {return { value: 0 }})
    let writer = rb.claim()
    writer.value = 20
    for (let i = 0; i < 4; i++) {
      rb.claim()
    }
    let reader = rb.peek(0)
    assert.equal(reader.value, 20, 'retain prior value due to no reset')
    assert.end()
  })

  suite.test('should reset elements wrapping if reset is provided', assert => {
    let rb = RingBuffer<ITestElement>(4, () => {return { value: 0 }}, T => T.value = 0)
    let writer = rb.claim()
    writer.value = 20
    for (let i = 0; i < 4; i++) {
      rb.claim()
    }
    let reader = rb.peek(0)
    assert.equal(reader.value, 0, 'reset value using provided function')
    assert.end()
  })

  suite.end()
})

test('ObjectRingBuffer<T>', suite => {

  interface ITestElement {
    value: number
  }

  suite.test('should create a buffer of proper length', assert => {
    let rb = ObjectRingBuffer<ITestElement>(4, (ring, index) => {return { value: index }})
    assert.equal(rb.getRingSize(), 4, 'be a 4 slice ring')
    assert.end()
  })

  suite.test('should not reset elements if no reset is provided', assert => {
    let rb = ObjectRingBuffer<ITestElement>(4, (ring, index) => {return { value: index }})
    let writer = rb.claim()
    writer.value = 20
    for (let i = 0; i < 4; i++) {
      rb.claim()
    }
    let reader = rb.peek(0)
    assert.equal(reader.value, 20, 'retain prior value due to no reset')
    assert.end()
  })

  suite.test('should reset elements wrapping if reset is provided', assert => {
    let rb = ObjectRingBuffer<ITestElement>(4, (ring, index) => {return { value: index }}, T => T.value = 0)
    let writer = rb.claim()
    writer.value = 20
    for (let i = 0; i < 4; i++) {
      rb.claim()
    }
    let reader = rb.peek(0)
    assert.equal(reader.value, 0, 'reset value using provided function')
    assert.end()
  })

  suite.end()
})

test('BinaryRingBuffer', suite => {

  suite.test('should create buffer with valid config params', assert => {
    let rb = BinaryRingBuffer(4, 4)
    assert.equal(rb.getRingSize(), 4, 'be 4 slice ring')
    assert.end()
  })

  suite.test('should convert non integer numbers to integer for ring size and slice size', assert => {
    let rb = BinaryRingBuffer(10.5, 0.4)
    assert.equal(rb.getRingSize(), 16, 'corrected to 2s complement')
    assert.end()
  })

  suite.test('should return valid claim and step write position', assert => {
    let rb = BinaryRingBuffer(4, 4)
    let handle = rb.claim()
    let position = rb.getPosition()

    assert.equal(handle.byteOffset, 0, 'return first position index')
    assert.equal(position, 1, 'moves to the next position')
    assert.end()
  })

  suite.test('should write to claim handle', assert => {
    let rb = BinaryRingBuffer(4, 4)
    let writer = rb.claim()
    writer.setInt8(0, 10)
    writer.setInt8(1, 20)

    assert.equal(writer.byteLength, 4, 'only return claimed slice')

    let reader = rb.peek(0)
    assert.equal(reader.getInt8(0), 10, 'read success first byte from 4x4')
    assert.equal(reader.getInt8(1), 20, 'read success second byte from 4x4')
    assert.end()
  })

  suite.test('should be able to make big rings', assert => {
    let rb = BinaryRingBuffer(64, 32)
    let writer = rb.claim()
    writer.setInt32(0, 10)

    assert.equal(writer.byteLength, 32, 'writer is the correct length')

    let reader = rb.peek(0)
    assert.equal(reader.getInt32(0), 10, 'sucessful read from 64 slice ring')
    assert.end()
  })

  suite.test('should wrap index and not position when rolling back around the ring', assert => {
    let rb = BinaryRingBuffer(4, 4)
    rb.claim()
    rb.claim()
    rb.claim()
    // check pre-wrap
    assert.equal(rb.getPosition(), 3, 'return correct position after first claims')
    assert.equal(rb.getIndex(), 3, 'return correct index after first claims')

    rb.claim()
    rb.claim()
    // check post-wrap
    assert.equal(rb.getPosition(), 5, 'return correct position after wrapping claims')
    assert.equal(rb.getIndex(), 1, 'return correct index after wrapping claims')
    assert.end()
  })

  suite.test('should be able to write to cursor position after wrapping large ring', assert => {
    let rb = BinaryRingBuffer(4, 32)
    for (let i = 0; i < 31; i++) {
      let writer = rb.claim()
      writer.setInt32(0, 10)
    }
    assert.end()
  })

  suite.test('should clear data by default', assert => {
    let rb = BinaryRingBuffer(4, 4)
    let writer = rb.claim()
    writer.setInt8(0, 10)
    for (let i = 0; i < 4; i++) {
      rb.claim() // roll through previously set item
    }
    let reader = rb.peek(0) // look at first element to check for remaining data
    assert.equal(reader.getInt8(0), 0, 'clear data from ring on each pass by default')
    assert.end()
  })

  suite.test('should clear data if flag is set as true', assert => {
    let rb = BinaryRingBuffer(4, 4, true)
    let writer = rb.claim()
    writer.setInt8(0, 10)
    for (let i = 0; i < 4; i++) {
      rb.claim() // roll through previously set item
    }
    let reader = rb.peek(0) // look at first element to check for remaining data
    assert.equal(reader.getInt8(0), 0, 'clear data from ring on each pass if flag is true')
    assert.end()
  })

  suite.test('should not clear data if flag is set as false', assert => {
    let rb = BinaryRingBuffer(4, 4, false)
    let writer = rb.claim()
    writer.setInt8(0, 10)
    for (let i = 0; i < 4; i++) {
      rb.claim() // roll through previously set item
    }
    let reader = rb.peek(0) // look at first element to check for remaining data
    assert.equal(reader.getInt8(0), 10, 'not clear data from ring if flag is false')
    assert.end()
  })

  suite.end()
})
