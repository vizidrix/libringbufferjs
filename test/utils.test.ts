import * as test from 'blue-tape'
import {
  ClampToCompliment,
  ClampToNextWholeNumber,
  FlushDataView,
} from '../src/libringbufferjs'

test('ClampToCompliment - Should round value up to 2, if less than 2, or next nearest compliment', assert => {
  const tests = [
    [-20, 2],
    [-10, 2],
    [-1, 2],
    [0, 2],
    [1, 2],
    [2, 2],
    [3, 4],
    [4, 4],
    [5, 8],
    [8, 8],
    [9, 16],
    [16, 16],
    [17, 32],
    [100, 128],
    [128, 128],
    [1000, 1024],
    [1024, 1024],
  ]
  const count = tests.length
  let i: number,
      value: number,
      actual: number,
      expected: number
  for (i = 0; i < count; i++) {
    value = tests[i][0]
    actual = ClampToCompliment(value)
    expected = tests[i][1]

    assert.equal(actual, expected, 'Complement of [ ' + value + ' ] was [ ' + actual + ' ]')
  }
  assert.end()
})

test('ClampToNextWholeNumber - Should round value up to 1, if less than 1, or nearest whole number', assert => {
  const tests = [
    [-20, 1],
    [-10, 1],
    [-1, 1],
    [0, 1],
    [1, 1],
    [2, 2],
    [20, 20],
    [0.1, 1],
    [1.2, 2],
    [100.4, 101],
  ]
  const count = tests.length
  let i: number,
      value: number,
      actual: number,
      expected: number
  for (i = 0; i < count; i++) {
    value = tests[i][0]
    actual = ClampToNextWholeNumber(value)
    expected = tests[i][1]

    assert.equal(actual, expected, 'Next whole of [ ' + value + ' ] was [ ' + actual + ' ]')
  }
  assert.end()
})

test('FlushDataView - Should reset provided data view to all zeros', assert => {
  let buffer = new ArrayBuffer(10)
  let view = new DataView(buffer, 0, 1)
  view.setInt8(0, 10)
  assert.equal(view.getInt8(0), 10, 'set the byte at position zero')
  FlushDataView(view)
  assert.equal(view.getInt8(0), 0, 'reset by Flush call')
  assert.end()
})
