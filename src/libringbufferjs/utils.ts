/**
 * Library: gitlab.com/vizidrix/libringbufferjs
 * File: /src/ringbufferjs/utils.ts
 * -
 */

/**
 * ClampToCompliment accepts a number and ensures that it is a 2s compliment.
 * If not, it rounds it up to the next 2s complement.
 * -
 * @param value Input value to round to next 2s compliment
 * @return Resulting value clamped to a  2s compliment
 * -
 */
export function ClampToCompliment(value: number): number {
  if (value < 2) { return 2 }
  value--;
  value |= value >> 1;
  value |= value >> 2;
  value |= value >> 4;
  value |= value >> 8;
  value |= value >> 16;
  value |= value >> 32;
  value++;
  return value;
}

/**
 * ClampToNextWholeNumber accepts any numeric value and ensures that it is positive.
 * Rounded to next whole number if a fraction is provided.
 * -
 * @param value Input value to round to next positive whole number
 * @return Resulting positive, whole number
 * -
 */
export function ClampToNextWholeNumber(value: number): number {
  if (value <= 0) { return 1 }
  return Math.ceil(value)
}

/**
 * FlushDataView runs across a DataView and clears all the bytes to zero
 * -
 * @param target is the DataView instance to clear
 */
export function FlushDataView(target: DataView): void {
  for (let i = 0; i < target.byteLength; i++) {
    target.setInt8(i, 0)
  }
}
