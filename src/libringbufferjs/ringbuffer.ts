/**
 * Library: gitlab.com/vizidrix/libringbufferjs
 * File: /src/ringbufferjs/ringbuffer.ts
 * -
 */

import {
  ClampToCompliment,
  ClampToNextWholeNumber,
  FlushDataView
} from './utils'

/**
 * IRingBuffer provides access methods to interact with the ring buffer structure
 */
export interface IRingBuffer<T> {
  /** returns a DataView of the current position and moves the position forward */
  claim: () => T
  /** returns the current bounded position used to index the ring internally */
  getIndex: () => number
  /** returns the current, perpetually incrementing, cursor position of the ring buffer */
  getPosition: () => number
  /** returns the ring's initialization data */
  getRingSize: () => number
  /** returns a slice cursor to the slice at the specified index within the ring */
  peek: (index: number) => T
}

/**
 * RingBuffer<T> makes a ring buffer using a standard array of type T with pre-allocated instances of T in
 * each ring slot.  Consistent struct instantiation via the factory and uniform types in the array should allow
 * the optimizer to create an efficient memory structure.
 * - This is a simplified variant of ObjectRingBuffer<T> for the most basic use case
 */
export function RingBuffer<T>(
  ringSize: number,
  elementFactory: () => T,
  elementReset?: (element: T) => void)
  : IRingBuffer<T> {
  return MakeRingBuffer(ringSize,
    elementFactory,
    elementReset)
}

/**
 * ObjectRingBuffer<T> makes a ring buffer using a standard array of type T with pre-allocated instances of T in
 * each ring slot.  Consistent struct instantiation via the factory and uniform types in the array should allow
 * the optimizer to create an efficient memory structure.
 * @generic T is the type of element in the ring slots, can be any object
 * -
 * @param ringSize determines the number of slices allocated in the ring buffer
 * @param elementFactory is used to build the initial ring elements
 * @param elementReset flushes any left over data to ensure clean reuse
 * @return ring buffer instance of type IRingBuffer<T>
 */
export function ObjectRingBuffer<T>(
  ringSize: number,
  elementFactory: (ring: Array<T>, index: number) => T,
  elementReset?: (element: T) => void,
  ringFactory?: (ringSize: number) => Array<T>)
  : IRingBuffer<T> {
  return MakeRingBuffer(ringSize,
    elementFactory,
    elementReset,
    ringFactory)
}

/**
 * BinaryRingBuffer makes a ring buffer using an ArrayBuffer for raw binary storage and DataViews for elements
 * -
 * @param ringSize determines the number of slices allocated in the ring buffer
 * @param sliceSize determines the size of the binary data element allocated for each ring slot
 * @return ring buffer instance of type IRingBuffer<DataView>
 */
export function BinaryRingBuffer(ringSize: number, sliceSize: number, resetElements?: boolean): IRingBuffer<DataView> {
  const sliceSizeTemp = ClampToNextWholeNumber(sliceSize)
  let reset = typeof resetElements === 'undefined' || resetElements ? FlushDataView : undefined
  return MakeRingBuffer(ringSize,
    (ring: ArrayBuffer, index: number) => new DataView(ring, index * sliceSize, sliceSize),
    reset,
    (ringSizeTemp: number) => new ArrayBuffer(ringSizeTemp * sliceSizeTemp))
}

/**
 * MakeRingBuffer<T, U> provides a generic template for ring buffer creation.
 * - Available but not intended for direct consumption, look at RingBuffer<T>, ObjectRingBuffer<T> or BinaryRingBuffer.
 * @generic T is the type of ring, defaults to Array<U> if not customized
 * @generic U is the type of element in the ring slots, can be any object
 * -
 * @param ringSize determines the number of slices allocated in the ring buffer
 * @param elementFactory is used to build the initial ring elements
 * @param elementReset flushes any left over data to ensure clean reuse
 * @param ringFactory allows an optional custom init function to create the base ring; must support indexing
 * @return ring buffer instance of type IRingBuffer<U>
 */
export function MakeRingBuffer<T, U>(
  ringSize: number,
  elementFactory: (ring: T | Array<U>, index: number) => U,
  elementReset?: (element: U) => void,
  ringFactory?: (ringSize: number) => T | Array<U>)
  : IRingBuffer<U> {
  const ringSizeTemp = ClampToCompliment(ringSize)
  const mask = ringSizeTemp - 1
  // make a standard array if a factory function wasn't provided
  const ring: T | Array<U> = typeof ringFactory === 'undefined' ? [] : ringFactory(ringSizeTemp)
  const elements: Array<U> = []
  for (let i = 0; i < ringSize; i++) {
    elements.push(elementFactory(ring, i))
  }
  let position = 0
  // skip reset if there wasn't a function provided
  const claim = typeof elementReset === 'undefined' ?
    () => elements[position++ & mask]
    :
    () => {
      elementReset(elements[position & mask])
      return elements[position++ & mask]
    }
  return {
    claim: claim,
    getIndex: () => position & mask,
    getPosition: () => position,
    getRingSize: () => ringSizeTemp,
    peek: index => elements[index & mask],
  }
}
